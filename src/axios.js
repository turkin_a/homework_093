import axios from 'axios';

const state = JSON.parse(localStorage.getItem('chat-state'));

const instance = axios.create({
  baseURL: 'http://localhost:8000',
  headers: {'x-access-token': state ? state.user.token : ''}
});

export default instance;