import * as actionTypes from '../actions/actionTypes';

const initState = {
  auth: false,
  registerError: false,
  token: '',
  user: {}
};

const userReducer = (state = initState, action) => {
  switch (action.type) {
    case actionTypes.REGISTER_USER_SUCCESS:
      return {...state, registerError: false};
    case actionTypes.REGISTER_USER_ERROR:
      console.log(action.error);
      return {...state};
    case actionTypes.FETCH_USER_DATA_SUCCESS:
      console.log(action.data);
      return {...state, auth: true, registerError: false, token: action.data.token, user: action.data.user};
    case actionTypes.USER_LOGOUT:
      return {...state, auth: false, token: '', user: {}};
    default:
      return state;
  }
};

export default userReducer;