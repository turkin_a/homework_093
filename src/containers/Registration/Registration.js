import React from 'react';
import {
  Alert, Button, Col, ControlLabel, FormControl, FormGroup, Grid, HelpBlock, PageHeader,
  Row
} from "react-bootstrap";
import {connect} from "react-redux";
import {registerUser} from "../../store/actions/user";

class Login extends React.Component {
  state = {
    username: '',
    password: '',
    passwordVerify: '',
    passwordsEqualError: false
  };

  inputHandler = event => {
    const name = event.target.name;
    const value = event.target.value;
    this.setState({[name]: value, passwordsEqualError: false})
  };

  registration = (event) => {
    event.preventDefault();

    if (this.state.password && this.state.password === this.state.passwordVerify) {
      this.props.registration({username: this.state.username, password: this.state.password})
    } else {
      this.setState({passwordsEqualError: 'Пароли не совпадают'});
    }
  };

  render() {
    return(
      <Grid>
        <Row>
          <Col lg={4} lgOffset={4} md={6} mdOffset={3}>
            <form onSubmit={this.registration}>
              <PageHeader>Регистрация</PageHeader>
              <h4>Пожалуйста заполните все поля</h4>
              <FormGroup>
                <ControlLabel>Имя</ControlLabel>
                <FormControl
                  placeholder="Введите имя пользователя"
                  name="username"
                  type="text"
                  value={this.state.username}
                  onChange={this.inputHandler}
                />
              </FormGroup>
              <FormGroup>
                <ControlLabel>Пароль</ControlLabel>
                <FormControl
                  placeholder="Введите пароль"
                  name="password"
                  type="password"
                  value={this.state.password}
                  onChange={this.inputHandler}
                />
              </FormGroup>
              <FormGroup>
                <ControlLabel>Повторите пароль</ControlLabel>
                <FormControl
                  placeholder="Повторите пароль"
                  name="passwordVerify"
                  type="password"
                  value={this.state.passwordVerify}
                  onChange={this.inputHandler}
                />
              </FormGroup>
              {this.state.passwordsEqualError &&
                <Alert bsStyle="danger">{this.state.passwordsEqualError}</Alert>
              }
              <Button bsStyle={'primary'} type={'submit'}>Зарегестрироваться</Button>
            </form>
          </Col>
        </Row>
      </Grid>
    )
  }
}

const initMapDispatchToProps = dispatch => {
  return {
    registration: (data) => dispatch(registerUser(data))
  }
};

export default connect(null, initMapDispatchToProps)(Login);