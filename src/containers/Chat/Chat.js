import React from 'react';
import {connect} from "react-redux";
import {Col, Grid, Row} from "react-bootstrap";

import UserList from "../../components/UserList/UserList";
import ChatRoom from "../../components/ChatRoom/ChatRoom";

class Chat extends React.Component{


  render() {
    return (
      <Grid>
        <Row>
          <Col md={3}>
            <UserList />
          </Col>
          <Col md={9}>
            <ChatRoom />
          </Col>
        </Row>
      </Grid>
    )
  }
}

const initMapStateToProps = state => ({

});

const initMapDispatchToProps = dispatch => ({

});

export default connect(initMapStateToProps, initMapDispatchToProps)(Chat);